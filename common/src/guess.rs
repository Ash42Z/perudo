use serde::{Deserialize, Serialize};

use std::cmp::Ordering;

#[derive(PartialEq, Deserialize, Serialize, Clone, Debug, Eq, Hash)]
pub struct Guess {
    pub count: usize,
    pub value: u8,
}

impl Guess {
    pub fn new(count: usize, value: u8) -> Self {
        Self { count, value }
    }

    pub fn is_valid(&self) -> bool {
        self.count >= 1 && self.value >= 1 && self.value <= 6
    }
}

impl std::fmt::Display for Guess {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} times {}", self.count, self.value)
    }
}

impl PartialOrd for Guess {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(match self.count.cmp(&other.count) {
            Ordering::Less => {
                if self.value == 1
                    && other.value != 1
                    && (self.count * 2 >= other.count)
                {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            }
            Ordering::Equal => {
                if self.value == 1 && other.value != 1 {
                    Ordering::Greater
                } else if self.value != 1 && other.value == 1 {
                    Ordering::Less
                } else if self.value > other.value {
                    Ordering::Greater
                } else if self.value < other.value {
                    Ordering::Less
                } else {
                    Ordering::Equal
                }
            }
            Ordering::Greater => match other.partial_cmp(self) {
                Some(Ordering::Less) => Ordering::Greater,
                Some(Ordering::Greater) => Ordering::Less,
                _ => unreachable!(),
            },
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_order() {
       let order = vec![
           Guess::new(1, 2),
           Guess::new(1, 3),
           Guess::new(1, 4),
           Guess::new(1, 5),
           Guess::new(1, 6),
           Guess::new(2, 2),
           Guess::new(2, 3),
           Guess::new(2, 4),
           Guess::new(2, 5),
           Guess::new(2, 6),
           Guess::new(1, 1),
           Guess::new(3, 2),
           Guess::new(3, 3),
           Guess::new(3, 4),
           Guess::new(3, 5),
           Guess::new(3, 6),
           Guess::new(4, 2),
           Guess::new(4, 3),
           Guess::new(4, 4),
           Guess::new(4, 5),
           Guess::new(4, 6),
           Guess::new(2, 1),
           Guess::new(5, 2),
           Guess::new(5, 3),
           Guess::new(5, 4),
           Guess::new(5, 5),
           Guess::new(5, 6),
           Guess::new(6, 2),
           Guess::new(6, 3),
           Guess::new(6, 4),
           Guess::new(6, 5),
           Guess::new(6, 6),
           Guess::new(3, 1),
       ];

       for idx in 0..order.len() {
           let guess = &order[idx];

           assert!(guess == guess);
           assert!(!(guess > guess));
           assert!(!(guess < guess));

           for before in &order[..idx] {
               assert!(!(before == guess));
               assert!(!(before > guess));
               assert!(before < guess);

               assert!(!(guess == before));
               assert!(guess > before);
               assert!(!(guess < before));
           }

           for after in &order[idx+1..] {
               assert!(!(after == guess));
               assert!(after > guess);
               assert!(!(after < guess));

               assert!(!(guess == after));
               assert!(!(guess > after));
               assert!(guess < after);
           }
       }
    }
}
