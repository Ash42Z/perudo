use crate::guess::Guess;
use crate::event::ChatMessage;

use serde::{Deserialize, Serialize};

#[derive(Clone)]
pub enum Routes {
    CreateGame,
    JoinGame,
    StartGame,
    NextEvents,
    Guess,
    LiftCup,
    Chat,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct CreateGameResponse {
    pub key: String,
    pub admin_key: String,
}

#[derive(Deserialize, Serialize)]
pub struct JoinGameParams {
    pub key: String,
    pub player_name: String,
}

#[derive(Deserialize, Serialize)]
pub struct NextEventsParams {
    pub key: String,
    pub from: usize,
}

#[derive(Serialize, Deserialize)]
pub struct GuessParams {
    pub key: String,
    pub guess: Guess,
}

#[derive(Serialize, Deserialize)]
pub struct ChatParams {
    pub key: String,
    pub message: ChatMessage,
}

impl AsRef<str> for Routes {
    fn as_ref(&self) -> &str {
        match self {
            Self::CreateGame => "create_game",
            Self::JoinGame => "join_game",
            Self::StartGame => "start_game",
            Self::NextEvents => "next_events",
            Self::Guess => "guess",
            Self::LiftCup => "lift_cup",
            Self::Chat => "chat",
        }
    }
}
