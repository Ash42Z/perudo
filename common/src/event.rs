use crate::dice::Dice;
use crate::guess::Guess;

use serde::{Deserialize, Serialize};

use std::collections::HashMap;

#[derive(Deserialize, Serialize, Clone, PartialEq, Eq, Debug)]
#[serde(rename_all = "snake_case", tag = "type")]
pub enum ChatMessage {
    DiceInfo { player: String, guess: Guess },
    ShouldLift,
    ShouldGuess { guess: Guess },
    FocusOn { player: String },
    Text { message: String },
}

impl std::fmt::Display for ChatMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::DiceInfo { player, guess } => {
                write!(f, "{} has {}", player, guess)
            }
            Self::ShouldLift => write!(f, "Lift the cup"),
            Self::ShouldGuess { guess } => write!(f, "Guess {}", guess),
            Self::FocusOn { player } => {
                write!(f, "We should focus on {}", player)
            }
            Self::Text { message } => write!(f, "{}", message),
        }
    }
}

#[derive(Deserialize, Serialize, Clone, PartialEq, Eq, Debug)]
#[serde(rename_all = "snake_case", tag = "type")]
pub enum Event {
    PlayerJoin {
        name: String,
    },
    RoundStarted {
        player: String,
        order: Vec<String>,
        dice: Dice,
        loser_round: bool,
    },
    Guessed {
        player: String,
        guess: Guess,
    },
    Lifted {
        player: String,
        dice: HashMap<String, Dice>,
        loser: String,
    },
    Timeout {
        player: String
    },
    Winner {
        player: String,
    },
    Chat {
        player: String,
        message: ChatMessage,
    },
}

impl std::fmt::Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::PlayerJoin { name } => {
                write!(f, "{} has joined", name)
            }
            Self::RoundStarted {
                player,
                order,
                dice,
                loser_round,
            } => {
                write!(f, "A new round has started.\n")?;
                write!(f, "Your name is {}.\n", player)?;

                write!(f, "The order is: ")?;
                for (idx, name) in order.iter().enumerate() {
                    if idx == order.len() - 1 {
                        write!(f, "{}.\n", name)?;
                    } else {
                        write!(f, "{}, ", name)?;
                    }
                }

                if dice.0.len() > 0 {
                    write!(f, "Your dice are: {}\n", dice)?;
                } else {
                    write!(f, "You have no dice left/\n")?;
                }

                if *loser_round {
                    write!(f, "This is a losers round.")
                } else {
                    write!(f, "This is not a losers round.")
                }
            }
            Self::Guessed { player, guess } => {
                write!(f, "{} guessed {}.", player, guess)
            }
            Self::Lifted { player, dice, loser } => {
                write!(f, "{} has lifted their cup.\n", player)?;
                for (name, dice) in dice.iter() {
                    if dice.0.len() > 0 {
                        write!(f, "{} had {}\n", name, dice)?;
                    } else {
                        write!(f, "{} had no dice\n", name)?;
                    }
                }
                let remaining_dice = dice[loser].0.len() - 1;
                write!(f, "{} has lost this round. ", loser)?;
                if remaining_dice == 0 {
                    write!(f, "They have been eliminated.\n")
                } else {
                    write!(f, "They have {} dice left.\n", remaining_dice)
                }
            }
            Self::Timeout { player } => {
                write!(f, "{} took too long and lost a die\n", player)
            }
            Self::Winner { player } => {
                write!(f, "{} has won the game.\n", player)
            },
            Self::Chat { player, message } => {
                write!(f, "{} says: \"{}\".\n", player, message)
            }
        }
    }
}
