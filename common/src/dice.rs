use rand::distributions::Uniform;
use rand::{thread_rng, Rng};

use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq, Debug, Default, Hash)]
pub struct Dice(pub Vec<u8>);

impl std::fmt::Display for Dice {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (idx, die) in self.0.iter().enumerate() {
            if idx == self.0.len() - 1 {
                write!(f, "{}.", die)?;
            } else {
                write!(f, "{} ", die)?;
            }
        }

        Ok(())
    }
}

impl Dice {
    pub fn roll(n: usize) -> Self {
        let dist = Uniform::from(1..=6);

        Self {
            0: thread_rng().sample_iter(&dist).take(n.into()).collect(),
        }
    }

    pub fn count(&self, i: u8, losers_round: bool) -> usize {
        if i < 1 || i > 6 {
            return 0;
        }

        self.0
            .iter()
            .filter(|n| (!losers_round && **n == 1) || **n == i)
            .count()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[test]
    fn test_roll() {
        for i in 0..=100 {
            let dice = Dice::roll(i);

            assert_eq!(dice.0.len(), i);

            for die in dice.0 {
                assert!(die >= 1 && die <= 6);
            }
        }
    }

    #[rstest]
    fn test_empty_count(
        #[values(false, true)] loser_round: bool,
        #[values(0, 1, 2, 3, 4, 5, 6, 7)] value: u8,
    ) {
        let dice = Dice { 0: vec![] };

        assert_eq!(dice.count(value, loser_round), 0);
    }

    #[rstest]
    fn test_ones_count(
        #[values(vec![1], vec![1, 1], vec![1, 1, 1])] dice: Vec<u8>,
    ) {
        let len = dice.len();
        let dice = Dice { 0: dice };

        assert_eq!(dice.count(0, false), 0);
        assert_eq!(dice.count(0, true), 0);
        assert_eq!(dice.count(7, false), 0);
        assert_eq!(dice.count(7, true), 0);

        assert_eq!(dice.count(1, false), len);
        assert_eq!(dice.count(1, true), len);

        for i in 2..=6 {
            assert_eq!(dice.count(i, false), len);
            assert_eq!(dice.count(i, true), 0);
        }
    }

    #[rstest]
    fn test_others_count(
        #[values(2, 3, 4, 5, 6)] value: u8,
        #[values(1, 2, 3)] count: u8,
    ) {
        let dice = Dice {
            0: std::iter::repeat(value).take(count.into()).collect(),
        };

        for i in 1..=6 {
            if i == value {
                assert_eq!(dice.count(i, false), count.into());
                assert_eq!(dice.count(i, true), count.into());
            } else {
                assert_eq!(dice.count(i, false), 0);
                assert_eq!(dice.count(i, true), 0);
            }
        }
    }

    #[rstest]
    // A
    #[case(vec![1, 2, 3, 4, 4], 1, false, 1)]
    #[case(vec![1, 2, 3, 4, 4], 1, true, 1)]
    #[case(vec![1, 2, 3, 4, 4], 2, false, 2)]
    #[case(vec![1, 2, 3, 4, 4], 2, true, 1)]
    #[case(vec![1, 2, 3, 4, 4], 3, false, 2)]
    #[case(vec![1, 2, 3, 4, 4], 3, true, 1)]
    #[case(vec![1, 2, 3, 4, 4], 4, false, 3)]
    #[case(vec![1, 2, 3, 4, 4], 4, true, 2)]
    #[case(vec![1, 2, 3, 4, 4], 5, false, 1)]
    #[case(vec![1, 2, 3, 4, 4], 5, true, 0)]
    #[case(vec![1, 2, 3, 4, 4], 6, false, 1)]
    #[case(vec![1, 2, 3, 4, 4], 6, true, 0)]
    // B
    #[case(vec![1, 1, 6, 6, 2], 1, false, 2)]
    #[case(vec![1, 1, 6, 6, 2], 1, true, 2)]
    #[case(vec![1, 1, 6, 6, 2], 2, false, 3)]
    #[case(vec![1, 1, 6, 6, 2], 2, true, 1)]
    #[case(vec![1, 1, 6, 6, 2], 3, false, 2)]
    #[case(vec![1, 1, 6, 6, 2], 3, true, 0)]
    #[case(vec![1, 1, 6, 6, 2], 4, false, 2)]
    #[case(vec![1, 1, 6, 6, 2], 4, true, 0)]
    #[case(vec![1, 1, 6, 6, 2], 5, false, 2)]
    #[case(vec![1, 1, 6, 6, 2], 5, true, 0)]
    #[case(vec![1, 1, 6, 6, 2], 6, false, 4)]
    #[case(vec![1, 1, 6, 6, 2], 6, true, 2)]

    fn test_specific_count(
        #[case] dice: Vec<u8>,
        #[case] value: u8,
        #[case] loser_round: bool,
        #[case] expected: usize,
    ) {
        let dice = Dice { 0: dice };
        assert_eq!(dice.count(value, loser_round), expected);
    }
}
