mod games;
use games::{GameError, Games};

mod game;
mod round;

use rand::distributions::Alphanumeric;

use warp::http::StatusCode;
use warp::reply::{json, with_status, Reply};
use warp::Filter;

use lazy_static::lazy_static;

use rand::{thread_rng, Rng};

use serde::Serialize;

use std::sync::RwLock;

use common::api::{
    ChatParams, GuessParams, JoinGameParams, NextEventsParams, Routes,
};

use common::event::Event;

lazy_static! {
    static ref GAMES: RwLock<Games> = RwLock::new(Default::default());
}

pub(crate) fn generate_key() -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(16)
        .map(char::from)
        .collect()
}

fn into_reply<T: Serialize>(res: Result<T, GameError>) -> Box<dyn Reply> {
    match res {
        Ok(t) => Box::new(warp::reply::json(&t)),
        Err(e) => Box::new(with_status(e.to_string(), StatusCode::FORBIDDEN)),
    }
}

async fn handle_timers() {
    loop {
        let now = std::time::SystemTime::now();

        for game in GAMES.write().unwrap().games.values_mut() {
            if game.round.is_none() {
                continue;
            };

            if let Some(timer) = game.turn_timer {
                if now.duration_since(timer).map_or(false, |diff| {
                    diff > std::time::Duration::from_secs(10)
                }) {
                    let current_player =
                        game.round.as_ref().unwrap().current_player;

                    let current_player_name = game.round.as_ref().unwrap().order
                        [current_player]
                        .clone();

                    game.broadcast(&Event::Timeout {
                        player: current_player_name.clone(),
                    });
                    let _ = game.handle_loser(current_player, &current_player_name);
                }
            }
        }
        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
    }
}

#[tokio::main]
async fn main() {
    let create_game = warp::post()
        .and(warp::path(Routes::CreateGame))
        .and(warp::path::end())
        .map(|| json(&GAMES.write().unwrap().create_game()));

    let join_game = warp::post()
        .and(warp::path(Routes::JoinGame))
        .and(warp::path::end())
        .and(warp::body::json())
        .map(|params: JoinGameParams| -> Box<dyn Reply> {
            into_reply(
                GAMES
                    .write()
                    .unwrap()
                    .add_player(params.key, params.player_name),
            )
        });

    let start_game = warp::post()
        .and(warp::path(Routes::StartGame))
        .and(warp::path::end())
        .and(warp::body::json())
        .map(|admin_key: String| -> Box<dyn Reply> {
            into_reply(GAMES.write().unwrap().start_game(admin_key))
        });

    let events = warp::post()
        .and(warp::path(Routes::NextEvents))
        .and(warp::path::end())
        .and(warp::body::json())
        .map(|params: NextEventsParams| -> Box<dyn Reply> {
            into_reply(
                GAMES.read().unwrap().next_events(params.key, params.from),
            )
        });

    let guess = warp::post()
        .and(warp::path(Routes::Guess))
        .and(warp::path::end())
        .and(warp::body::json())
        .map(|params: GuessParams| -> Box<dyn Reply> {
            into_reply(GAMES.write().unwrap().guess(params.key, params.guess))
        });

    let lift = warp::post()
        .and(warp::path(Routes::LiftCup))
        .and(warp::path::end())
        .and(warp::body::json())
        .map(|key: String| -> Box<dyn Reply> {
            into_reply(GAMES.write().unwrap().lift(key))
        });

    let chat = warp::post()
        .and(warp::path(Routes::Chat))
        .and(warp::path::end())
        .and(warp::body::json())
        .map(|params: ChatParams| -> Box<dyn Reply> {
            into_reply(GAMES.write().unwrap().chat(params.key, params.message))
        });

    let index = warp::path::end().and(warp::fs::file("./dist/index.html"));

    tokio::spawn(handle_timers());

    warp::serve(
        create_game
            .or(join_game)
            .or(start_game)
            .or(events)
            .or(guess)
            .or(lift)
            .or(chat)
            .or(warp::fs::dir("./dist"))
            .or(index),
    )
    .run(([0, 0, 0, 0], 80))
    .await;
}
