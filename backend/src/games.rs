use crate::game::Game;
use crate::generate_key;

use common::api::CreateGameResponse;
use common::event::{ChatMessage, Event};
use common::guess::Guess;

use thiserror::Error;

use std::collections::HashMap;

#[derive(Default)]
pub(crate) struct Games {
    // Key -> Game
    pub(crate) games: HashMap<String, Game>,
    // Key -> Game key
    pub(crate) admins: HashMap<String, String>,
    // Key -> (Game key, Player name)
    pub(crate) players: HashMap<String, (String, String)>,
}

#[derive(Error, Debug, PartialEq, Eq)]
pub(crate) enum GameError {
    #[error("Key {0} does not exist")]
    BadKey(String),
    #[error("Player {0} does not exist")]
    BadPlayerName(String),
    #[error("Current game state cannot support the action attempted")]
    WrongGameState,
    #[error("Attempted to add a player with a name that already exists")]
    PlayerExists,
    #[error("Tried to start a game with less than 2 players")]
    NotEnoughPlayers,
    #[error("Tries to guess an invalid guess for this game state")]
    BadGuess,
    #[error("Received a bad paramter from the user")]
    BadParameter,
}

impl Games {
    pub(crate) fn create_game(&mut self) -> CreateGameResponse {
        let game_key = generate_key();
        let admin_key = generate_key();

        self.games.insert(game_key.clone(), Game::default());
        self.admins.insert(admin_key.clone(), game_key.clone());

        CreateGameResponse {
            key: game_key,
            admin_key,
        }
    }

    pub(crate) fn add_player(
        &mut self,
        game_key: String,
        player_name: String,
    ) -> Result<String, GameError> {
        let player_key = generate_key();

        let game = self
            .games
            .get_mut(&game_key)
            .ok_or_else(|| GameError::BadKey(game_key.clone()))?;

        if game.winner.is_some() || game.round.is_some() {
            return Err(GameError::WrongGameState);
        }

        if game.player_events.get(&player_name).is_some() {
            return Err(GameError::PlayerExists);
        }

        game.add_player(player_name.clone())?;

        self.players
            .insert(player_key.clone(), (game_key, player_name));

        Ok(player_key)
    }

    pub(crate) fn start_game(
        &mut self,
        admin_key: String,
    ) -> Result<(), GameError> {
        let Some(game_key) = self.admins.get(&admin_key) else {
            return Err(GameError::BadKey(admin_key));
        };

        self.games.get_mut(game_key).unwrap().start()
    }

    pub(crate) fn next_events(
        &self,
        key: String,
        from: usize,
    ) -> Result<Vec<Event>, GameError> {
        if let Some(game_key) = self.admins.get(&key) {
            self.games.get(game_key).unwrap().next_admin_events(from)
        } else if let Some((game_key, player_name)) = self.players.get(&key) {
            self.games
                .get(game_key)
                .unwrap()
                .next_player_events(player_name.clone(), from)
        } else {
            Err(GameError::BadKey(key))
        }
    }

    pub(crate) fn guess(
        &mut self,
        key: String,
        guess: Guess,
    ) -> Result<(), GameError> {
        let Some((game_key, player_name)) = self.players.get(&key) else {
            return Err(GameError::BadKey(key));
        };

        self.games
            .get_mut(game_key)
            .unwrap()
            .guess(player_name.clone(), guess)
    }

    pub(crate) fn lift(&mut self, key: String) -> Result<(), GameError> {
        let Some((game_key, player_name)) = self.players.get(&key) else {
            return Err(GameError::BadKey(key));
        };

        self.games
            .get_mut(game_key)
            .unwrap()
            .lift(player_name.clone())
    }

    pub(crate) fn chat(
        &mut self,
        key: String,
        message: ChatMessage,
    ) -> Result<(), GameError> {
        let Some((game_key, player_name)) = self.players.get(&key) else {
            return Err(GameError::BadKey(key));
        };
        let event = Event::Chat {
            player: player_name.clone(),
            message,
        };

        for (name, evs) in self
            .games
            .get_mut(game_key)
            .unwrap()
            .player_events
            .iter_mut()
        {
            if name != player_name {
                evs.push(event.clone());
            }
        }

        self.games.get_mut(game_key).unwrap().admin_events.push(event.clone());

        Ok(())
    }
}
