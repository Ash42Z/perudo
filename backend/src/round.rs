// This module is used exclusively by `game.rs`, so it's functionality
// is tested there.
use crate::games::GameError;
use common::{dice::Dice, event::Event, guess::Guess};

use std::collections::HashMap;

#[derive(Clone, Default)]
pub(crate) struct Round {
    pub(crate) order: Vec<String>,
    pub(crate) dice: HashMap<String, Dice>,
    pub(crate) current_player: usize,
    pub(crate) current_guess: Option<Guess>,
    pub(crate) loser_round: bool,
}

impl Round {
    pub(crate) fn determine_loser_on_lift(&self) -> Result<usize, GameError> {
        let Some(current_guess) = &self.current_guess else {
            return Err(GameError::WrongGameState);
        };

        Ok(
            if self
                .dice
                .values()
                .map(|d| Ok(d.count(current_guess.value, self.loser_round)))
                .collect::<Result<Vec<_>, _>>()?
                .into_iter()
                .sum::<usize>()
                < current_guess.count
            {
                if self.current_player == 0 {
                    self.order.len() - 1
                } else {
                    self.current_player - 1
                }
            } else {
                self.current_player
            },
        )
    }

    pub(crate) fn setup_next(&mut self, loser: usize) {
        self.order.rotate_left(loser);
        self.current_player = 0;
        self.current_guess = None;

        let loser_name = self.order[0].clone();
        let loser_dice_count = self.dice[&loser_name].0.len();

        self.loser_round = loser_dice_count == 2 && self.order.len() > 2;

        if loser_dice_count == 1 {
            self.order.remove(0);
        }

        for (player, dice) in self.dice.iter_mut() {
            let mut count = dice.0.len();
            if player == &loser_name {
                count = std::cmp::max(count - 1, 0);
            }
            *dice = Dice::roll(count);
        }
    }

    pub(crate) fn start_events(&self) -> HashMap<String, Event> {
        self.order
            .iter()
            .cloned()
            .map(|player| {
                (
                    player.clone(),
                    Event::RoundStarted {
                        player: player.clone(),
                        order: self.order.clone(),
                        dice: self.dice[&player].clone(),
                        loser_round: self.loser_round,
                    },
                )
            })
            .collect()
    }
}
