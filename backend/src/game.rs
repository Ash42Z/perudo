use crate::games::GameError;
use crate::round::Round;

use common::{dice::Dice, event::Event, guess::Guess};

use itertools::Itertools;

use rand::seq::SliceRandom;
use rand::thread_rng;

use std::collections::HashMap;
use std::time::SystemTime;

#[derive(Default)]
pub(crate) struct Game {
    pub(crate) admin_events: Vec<Event>,
    pub(crate) player_events: HashMap<String, Vec<Event>>,
    pub(crate) round: Option<Round>,
    pub(crate) winner: Option<String>,
    pub(crate) turn_timer: Option<SystemTime>,
}

impl Game {
    fn reset_timer(&mut self) {
        self.turn_timer = Some(SystemTime::now());
    }

    pub(crate) fn broadcast(&mut self, event: &Event) {
        Self::broadcast_partial_ref(
            event,
            &mut self.admin_events,
            &mut self.player_events,
        );
    }

    fn broadcast_partial_ref(
        event: &Event,
        admin: &mut Vec<Event>,
        players: &mut HashMap<String, Vec<Event>>,
    ) {
        admin.push(event.clone());
        for ev in players.values_mut() {
            ev.push(event.clone());
        }
    }

    pub(crate) fn add_player(
        &mut self,
        player_name: String,
    ) -> Result<(), GameError> {
        if self.winner.is_some() || self.round.is_some() {
            return Err(GameError::WrongGameState);
        }

        if self.player_events.get(&player_name).is_some() {
            return Err(GameError::PlayerExists);
        }

        self.player_events.insert(player_name.clone(), vec![]);
        self.broadcast(&Event::PlayerJoin { name: player_name });

        Ok(())
    }

    pub(crate) fn start(&mut self) -> Result<(), GameError> {
        if self.player_events.len() < 2 {
            return Err(GameError::NotEnoughPlayers);
        }

        if self.round.is_some() || self.winner.is_some() {
            return Err(GameError::WrongGameState);
        }

        let mut order: Vec<String> =
            self.player_events.keys().cloned().collect();

        order.shuffle(&mut thread_rng());

        let dice: HashMap<String, Dice> = order
            .iter()
            .cloned()
            .map(|name| (name, Dice::roll(5)))
            .collect();

        self.round = Some(Round {
            order: order.clone(),
            dice,
            current_player: 0,
            current_guess: None,
            loser_round: false,
        });

        let events = self.round.as_ref().unwrap().start_events();
        self.admin_events.extend(events.values().cloned());

        for (player, event) in events.into_iter() {
            self.player_events.get_mut(&player).unwrap().push(event)
        }

        self.reset_timer();

        Ok(())
    }

    pub(crate) fn next_admin_events(
        &self,
        from: usize,
    ) -> Result<Vec<Event>, GameError> {
        Ok(self
            .admin_events
            .get(from..)
            .ok_or(GameError::BadParameter)?
            .iter()
            .cloned()
            .collect())
    }

    pub(crate) fn next_player_events(
        &self,
        player_name: String,
        from: usize,
    ) -> Result<Vec<Event>, GameError> {
        Ok(self
            .player_events
            .get(&player_name)
            .ok_or_else(|| GameError::BadPlayerName(player_name.clone()))?
            .get(from..)
            .ok_or(GameError::BadParameter)?
            .iter()
            .cloned()
            .collect())
    }

    pub(crate) fn guess(
        &mut self,
        player_name: String,
        guess: Guess,
    ) -> Result<(), GameError> {
        let Some(round) = self.round.as_mut() else {
            return Err(GameError::WrongGameState);
        };

        if round.order[round.current_player] != player_name.as_str() {
            return Err(GameError::WrongGameState);
        }

        if !guess.is_valid() {
            return Err(GameError::BadGuess);
        }

        match &round.current_guess {
            Some(g)
                if !round.loser_round && guess <= *g
                    || (round.loser_round && guess.value != g.value)
                    || (round.loser_round && guess.count <= g.count) =>
            {
                Err(GameError::BadGuess)
            }
            None | Some(_) => {
                if round.current_guess.is_none()
                    && !round.loser_round
                    && guess.value == 1
                {
                    return Err(GameError::BadGuess);
                }

                round.current_player =
                    (round.current_player + 1) % round.order.len();
                round.current_guess = Some(guess.clone());

                self.broadcast(&Event::Guessed {
                    player: player_name.clone(),
                    guess,
                });

                self.reset_timer();

                Ok(())
            }
        }
    }

    pub(crate) fn handle_loser(
        &mut self,
        loser: usize,
        loser_name: &String,
    ) -> Result<(), GameError> {
        let Some(round) = self.round.as_mut() else {
            return Err(GameError::WrongGameState);
        };

        let remaining_dice = round.dice[loser_name].0.len() - 1;

        if remaining_dice == 0 && round.order.len() == 2 {
            let winner = (loser + 1) % 2;
            Self::broadcast_partial_ref(
                &Event::Winner {
                    player: round.order[winner].clone(),
                },
                &mut self.admin_events,
                &mut self.player_events,
            );
            self.round = None;
            self.turn_timer = None;
        } else {
            round.setup_next(loser);

            for (player_name, dice) in round.dice.iter_mut() {
                let ev = Event::RoundStarted {
                    player: player_name.clone(),
                    order: round.order.clone(),
                    dice: dice.clone(),
                    loser_round: round.loser_round,
                };

                self.player_events
                    .get_mut(player_name)
                    .unwrap()
                    .push(ev.clone());
                self.admin_events.push(ev);
            }

            self.reset_timer();
        }

        Ok(())

    }

    pub(crate) fn lift(
        &mut self,
        player_name: String,
    ) -> Result<(), GameError> {
        let Some(round) = self.round.as_mut() else {
            return Err(GameError::WrongGameState);
        };

        if round.order[round.current_player] != player_name.as_str() {
            return Err(GameError::WrongGameState);
        }

        if round.current_guess.is_none() {
            return Err(GameError::WrongGameState);
        };

        let loser = round.determine_loser_on_lift()?;

        let loser_name = &round.order[loser].clone();

        Self::broadcast_partial_ref(
            &Event::Lifted {
                player: player_name.clone(),
                dice: round.dice.clone(),
                loser: loser_name.clone(),
            },
            &mut self.admin_events,
            &mut self.player_events,
        );

        self.handle_loser(loser, &loser_name)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use rstest::rstest;

    use std::collections::HashSet;

    fn game_with_players(num: usize) -> Game {
        let mut game = Game::default();
        for i in 1..=num {
            game.player_events.insert(i.to_string(), vec![]);
        }
        game
    }

    fn check_events(
        game: &Game,
        events: Vec<Event>,
        expected_player_count: usize,
    ) {
        assert_eq!(game.player_events.len(), expected_player_count);
        assert_eq!(game.admin_events, events);
        for ev in game.player_events.values().cloned() {
            assert_eq!(ev, events);
        }
    }

    #[test]
    fn test_broadcast() {
        let ev = Event::Winner {
            player: "winner".into(),
        };
        let mut game = game_with_players(0);
        game.broadcast(&ev);
        check_events(&game, vec![ev.clone()], 0);

        let mut game = game_with_players(3);
        game.broadcast(&ev);
        check_events(&game, vec![ev.clone()], 3);
    }

    #[test]
    fn test_broadcast_partial() {
        let ev = Event::Winner {
            player: "winner".into(),
        };
        let mut game = game_with_players(0);
        Game::broadcast_partial_ref(
            &ev,
            &mut game.admin_events,
            &mut game.player_events,
        );
        check_events(&game, vec![ev.clone()], 0);

        let mut game = game_with_players(3);
        Game::broadcast_partial_ref(
            &ev,
            &mut game.admin_events,
            &mut game.player_events,
        );
        check_events(&game, vec![ev.clone()], 3);
    }

    #[test]
    fn test_add_player() {
        let mut game = Game::default();
        let mut evs: Vec<Event> = vec![];

        // Happy flow 1
        assert!(game.add_player("1".into()).is_ok());
        evs.push(Event::PlayerJoin { name: "1".into() });
        check_events(&game, evs.clone(), 1);

        // Happy flow 2
        assert!(game.add_player("2".into()).is_ok());
        evs.push(Event::PlayerJoin { name: "2".into() });
        let all_evs =
            [("1".into(), evs.clone()), ("2".into(), evs[1..].to_vec())]
                .into_iter()
                .collect::<HashMap<String, Vec<Event>>>();
        assert_eq!(game.admin_events, evs);
        assert_eq!(game.player_events, all_evs);

        // Duplicate name
        assert!(game.add_player("2".into()).is_err());
        assert_eq!(game.admin_events, evs);
        assert_eq!(game.player_events, all_evs);

        // Winner exists
        let mut game = Game::default();
        game.winner = Some("1".into());
        assert!(game.add_player("1".into()).is_err());
        check_events(&game, vec![], 0);

        // Round exists
        let mut game = Game::default();
        game.round = Some(Round::default());
        assert!(game.add_player("test".into()).is_err());
        check_events(&game, vec![], 0);
    }

    #[test]
    fn test_start() {
        // Minimum 2 players
        for player_count in 0..=1 {
            let mut game = game_with_players(player_count);
            assert!(game.start().is_err());
            check_events(&game, vec![], player_count);
        }

        // No winner or round in progress
        let mut game = game_with_players(3);
        game.winner = Some("test".into());
        assert!(game.start().is_err());
        check_events(&game, vec![], 3);

        let mut game = game_with_players(3);
        game.round = Some(Round::default());
        assert!(game.start().is_err());
        check_events(&game, vec![], 3);

        // Make sure games are started with the correct state
        for player_count in 2..=10 {
            let mut game = game_with_players(player_count);

            assert!(game.start().is_ok());
            assert!(game.round.is_some());
            let round = game.round.as_ref().unwrap();
            assert_eq!(
                (1..=player_count).into_iter().collect::<HashSet<_>>(),
                round
                    .order
                    .iter()
                    .map(|name| name.parse::<usize>().unwrap())
                    .collect::<HashSet<_>>()
            );

            assert_eq!(round.dice.len(), player_count);
            assert!(round.dice.values().all(|dice| dice.0.len() == 5));
            assert_eq!(round.current_player, 0);
            assert!(round.current_guess.is_none());
            assert!(!round.loser_round);

            let events = round.start_events();

            assert_eq!(game.admin_events.len(), events.len());
            for (player, ev) in events {
                let Event::RoundStarted {player: _, order, dice, loser_round} = &ev else {
                panic!("Unexpected event");
            };
                assert_eq!(order, &round.order);
                assert_eq!(dice, &round.dice[&player]);
                assert!(!loser_round);
                assert!(game.admin_events.contains(&ev));
                assert_eq!(game.player_events[&player], vec![ev]);
            }
        }

        // Make sure players are shuffled
        let mut game = game_with_players(100);
        game.start().unwrap();
        let round = game.round.as_ref().unwrap();
        assert_ne!(
            round.order,
            (1..=100)
                .into_iter()
                .map(|i| i.to_string())
                .collect::<Vec<_>>()
        );
        let dice = round.dice.values().cloned().collect::<Vec<_>>();
        assert!(dice[1..].iter().any(|d| d != &dice[0]));
    }

    #[test]
    fn test_next_events() {
        let mut game = game_with_players(3);

        assert_eq!(game.next_admin_events(0), Ok(vec![]));
        assert_eq!(game.next_player_events("1".into(), 0), Ok(vec![]));
        assert!(game.next_player_events("nope".into(), 0).is_err());

        for i in 1..=10 {
            assert!(game.next_admin_events(i).is_err());
            assert!(game.next_player_events("1".into(), i).is_err());
            assert!(game.next_player_events("nope".into(), i).is_err());
        }

        let evs = vec![
            Event::PlayerJoin { name: "1".into() },
            Event::PlayerJoin { name: "2".into() },
            Event::PlayerJoin { name: "3".into() },
        ];

        game.admin_events = evs.clone();
        *game.player_events.get_mut("1").unwrap() = evs.clone();

        assert_eq!(game.next_admin_events(0).unwrap(), evs);
        assert_eq!(game.next_player_events("1".into(), 0).unwrap(), evs);
        assert_eq!(game.next_player_events("2".into(), 0), Ok(vec![]));
        assert_eq!(game.next_player_events("3".into(), 0), Ok(vec![]));
        assert!(game.next_player_events("nope".into(), 0).is_err());

        assert_eq!(game.next_admin_events(2).unwrap(), evs[2..]);
        assert_eq!(game.next_player_events("1".into(), 2).unwrap(), evs[2..]);
        assert!(game.next_player_events("2".into(), 2).is_err());
        assert!(game.next_player_events("3".into(), 2).is_err());
        assert!(game.next_player_events("nope".into(), 2).is_err());

        assert_eq!(game.next_admin_events(3), Ok(vec![]));
        assert_eq!(game.next_player_events("1".into(), 3), Ok(vec![]));
        assert!(game.next_player_events("2".into(), 3).is_err());
        assert!(game.next_player_events("3".into(), 3).is_err());
        assert!(game.next_player_events("nope".into(), 3).is_err());

        assert!(game.next_admin_events(4).is_err());
        assert!(game.next_player_events("1".into(), 4).is_err());
        assert!(game.next_player_events("2".into(), 4).is_err());
        assert!(game.next_player_events("3".into(), 4).is_err());
        assert!(game.next_player_events("nope".into(), 4).is_err());
    }

    #[test]
    fn test_guess_checks() {
        // Game that has not started
        let mut game = game_with_players(3);
        assert!(game.guess("1".into(), Guess::new(1, 2)).is_err());
        check_events(&game, vec![], 3);

        // Not current player
        game.round = Some(Round {
            order: vec!["1".into(), "2".into(), "3".into()],
            dice: Default::default(),
            current_guess: None,
            current_player: 1,
            loser_round: false,
        });
        assert!(game.guess("1".into(), Guess::new(1, 2)).is_err());
        check_events(&game, vec![], 3);

        // 1 as first guess
        assert!(game.guess("2".into(), Guess::new(1, 1)).is_err());
        check_events(&game, vec![], 3);
    }

    #[rstest]
    #[case(Guess::new(2, 1), Guess::new(1, 2), false)]
    #[case(Guess::new(2, 6), Guess::new(2, 5), false)]
    #[case(Guess::new(2, 6), Guess::new(1, 3), false)]
    #[case(Guess::new(2, 1), Guess::new(2, 1), false)]
    #[case(Guess::new(2, 1), Guess::new(2, 2), true)]
    #[case(Guess::new(2, 1), Guess::new(1, 2), true)]
    #[case(Guess::new(2, 1), Guess::new(1, 1), true)]
    #[case(Guess::new(2, 1), Guess::new(2, 1), true)]
    #[case(Guess::new(2, 2), Guess::new(1, 3), true)]
    fn test_bad_guesses(
        #[case] current: Guess,
        #[case] next: Guess,
        #[case] loser: bool,
    ) {
        let mut game = game_with_players(3);
        game.round = Some(Round {
            order: vec!["1".into(), "2".into(), "3".into()],
            dice: Default::default(),
            current_guess: Some(current),
            current_player: 0,
            loser_round: loser,
        });

        assert!(game.guess("1".into(), next).is_err());
        check_events(&game, vec![], 3);
    }

    #[rstest]
    #[case(None, Guess::new(1, 2), false)]
    #[case(None, Guess::new(3, 4), false)]
    #[case(None, Guess::new(1, 2), true)]
    #[case(None, Guess::new(3, 4), true)]
    #[case(Some(Guess::new(1, 2)), Guess::new(1, 3), false)]
    #[case(Some(Guess::new(1, 2)), Guess::new(2, 2), false)]
    #[case(Some(Guess::new(1, 2)), Guess::new(2, 3), false)]
    #[case(Some(Guess::new(1, 2)), Guess::new(1, 1), false)]
    #[case(Some(Guess::new(1, 2)), Guess::new(2, 2), true)]
    #[case(Some(Guess::new(1, 2)), Guess::new(3, 2), true)]
    #[case(Some(Guess::new(2, 1)), Guess::new(3, 1), true)]
    fn test_good_guesses(
        #[case] current: Option<Guess>,
        #[case] next: Guess,
        #[case] loser: bool,
    ) {
        let mut game = game_with_players(3);
        game.round = Some(Round {
            order: vec!["1".into(), "2".into(), "3".into()],
            dice: Default::default(),
            current_guess: current,
            current_player: 0,
            loser_round: loser,
        });

        assert!(game.guess("1".into(), next.clone()).is_ok());
        let round = game.round.as_ref().unwrap();
        assert_eq!(round.current_player, 1);
        assert_eq!(round.current_guess, Some(next.clone()));

        check_events(
            &game,
            vec![Event::Guessed {
                player: "1".into(),
                guess: next,
            }],
            3,
        );
    }

    #[test]
    fn test_lift_checks() {
        // Game that has not started
        let mut game = game_with_players(3);
        assert!(game.lift("1".into()).is_err());
        check_events(&game, vec![], 3);

        // Not current player
        game.round = Some(Round {
            order: vec!["1".into(), "2".into(), "3".into()],
            dice: Default::default(),
            current_guess: None,
            current_player: 1,
            loser_round: false,
        });
        assert!(game.lift("1".into()).is_err());
        check_events(&game, vec![], 3);

        // No guess
        assert!(game.lift("2".into()).is_err());
        check_events(&game, vec![], 3);
    }

    #[rstest]
    // 1 Die each, 1
    #[case(vec![1], 1, Guess::new(1, 1), false, false)]
    #[case(vec![1], 1, Guess::new(2, 1), false, false)]
    #[case(vec![1], 1, Guess::new(3, 1), false, true)]
    #[case(vec![1], 1, Guess::new(1, 1), true, false)]
    #[case(vec![1], 1, Guess::new(2, 1), true, false)]
    #[case(vec![1], 1, Guess::new(3, 1), true, true)]
    // 1 Die each, 3
    #[case(vec![3], 3, Guess::new(1, 3), false, false)]
    #[case(vec![3], 3, Guess::new(2, 3), false, false)]
    #[case(vec![3], 3, Guess::new(3, 3), false, true)]
    #[case(vec![3], 3, Guess::new(1, 3), true, false)]
    #[case(vec![3], 3, Guess::new(2, 3), true, false)]
    #[case(vec![3], 3, Guess::new(3, 3), true, true)]
    // 1 Die each, mixed
    #[case(vec![1], 3, Guess::new(1, 3), false, false)]
    #[case(vec![1], 3, Guess::new(2, 3), false, false)]
    #[case(vec![1], 3, Guess::new(3, 3), false, true)]
    #[case(vec![1], 3, Guess::new(1, 3), true, false)]
    #[case(vec![1], 3, Guess::new(2, 3), true, true)]
    #[case(vec![1], 3, Guess::new(3, 3), true, true)]
    // 1 Die each, mixed, reverse order
    #[case(vec![3], 1, Guess::new(1, 3), false, false)]
    #[case(vec![3], 1, Guess::new(2, 3), false, false)]
    #[case(vec![3], 1, Guess::new(3, 3), false, true)]
    #[case(vec![3], 1, Guess::new(1, 3), true, false)]
    #[case(vec![3], 1, Guess::new(2, 3), true, true)]
    #[case(vec![3], 1, Guess::new(3, 3), true, true)]
    // Mixed dice
    #[case(vec![1, 1, 3, 5], 3, Guess::new(1, 3), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(2, 3), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(3, 3), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(4, 3), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(5, 3), false, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(1, 5), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(2, 5), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(3, 5), false, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(4, 5), false, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(5, 5), false, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(1, 3), true, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(2, 3), true, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(3, 3), true, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(4, 3), true, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(5, 3), true, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(1, 5), true, false)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(2, 5), true, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(3, 5), true, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(4, 5), true, true)]
    #[case(vec![1, 1, 3, 5], 3, Guess::new(5, 5), true, true)]
    fn test_lift_win(
        #[values(0, 1)] lifter: usize,
        #[case] winner_dice: Vec<u8>,
        #[case] loser_die: u8,
        #[case] guess: Guess,
        #[case] loser_round: bool,
        #[case] lifter_wins: bool,
    ) {
        let mut game = game_with_players(2);

        // 2 is the losing player
        let dice = [
            ("1".into(), Dice { 0: winner_dice }),
            ("2".into(), Dice { 0: vec![loser_die] }),
        ]
        .into_iter()
        .collect::<HashMap<String, Dice>>();

        // Choose where the winner and loser are based parameters
        let order: Vec<String> =
            if (lifter == 0 && lifter_wins) || (lifter == 1 && !lifter_wins) {
                vec!["1".into(), "2".into()]
            } else {
                vec!["2".into(), "1".into()]
            };

        let lifter_name = order[lifter].clone();

        game.round = Some(Round {
            order,
            dice: dice.clone(),
            current_guess: Some(guess),
            current_player: lifter,
            loser_round,
        });

        assert!(game.lift(lifter_name.clone()).is_ok());

        let evs = vec![
            Event::Lifted {
                player: lifter_name.clone(),
                dice,
                loser: "2".into(),
            },
            Event::Winner { player: "1".into() },
        ];

        check_events(&game, evs, 2);
    }

    #[rstest]
    // 2 Player with 3 dice
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(2, 3), false, false)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(2, 3), true, true)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(3, 3), false, true)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(2, 3), true, true)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(1, 1), false, false)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(1, 1), true, false)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(2, 1), false, true)]
    #[case(vec![vec![1, 2, 3], vec![4, 5, 6]], Guess::new(2, 1), true, true)]
    // 4 Players, mixed
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(3, 2), false, false
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(3, 2), true, false
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(6, 2), false, false
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(6, 2), true, true
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(8, 2), false, true
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(8, 2), true, true
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(3, 1), false, false
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(3, 1), true, false
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(5, 1), false, true
    )]
    #[case(vec![
           vec![3, 2], vec![5, 1, 1, 2], vec![6], vec![2, 4, 4, 1, 6]
        ], Guess::new(5, 1), true, true
    )]
    fn test_lift_next_round(
        #[case] dice: Vec<Vec<u8>>,
        #[case] current_guess: Guess,
        #[case] loser_round: bool,
        #[case] lifter_wins: bool,
    ) {
        let mut game = game_with_players(dice.len());

        let dice: HashMap<String, Dice> = dice
            .into_iter()
            .enumerate()
            .map(|(i, v)| ((i + 1).to_string(), Dice { 0: v }))
            .collect();

        let player_names =
            game.player_events.keys().cloned().collect::<Vec<_>>();

        for order in player_names.into_iter().permutations(dice.len()) {
            for player in 0..order.len() {
                let loser_idx = if lifter_wins {
                    if player == 0 {
                        order.len() - 1
                    } else {
                        player - 1
                    }
                } else {
                    player
                };

                let losing_name = order[loser_idx].clone();

                if dice[&losing_name].0.len() == 1 {
                    continue;
                }

                game.round = Some(Round {
                    order: order.clone(),
                    dice: dice.clone(),
                    current_guess: Some(current_guess.clone()),
                    current_player: player,
                    loser_round,
                });

                assert!(game.lift(order[player].clone()).is_ok());

                assert!(game.round.is_some());
                let round = game.round.as_ref().unwrap();

                assert!(round.current_guess.is_none());
                let mut new_order = order.clone();
                assert_eq!(round.current_player, 0);
                new_order.rotate_left(loser_idx);

                assert_eq!(round.order, new_order);

                let base_evs = vec![Event::Lifted {
                    player: order[player].clone(),
                    dice: dice.clone(),
                    loser: losing_name.clone(),
                }];

                let mut admin_evs = base_evs.clone();

                let loser_round = (dice[&losing_name].0.len() - 1) == 1;

                for (player_name, player_ev) in game.player_events.iter_mut() {
                    let mut ev = base_evs.clone();

                    let expected_dice_count = if player_name == &losing_name {
                        dice[player_name].0.len() - 1
                    } else {
                        dice[player_name].0.len()
                    };

                    let player_dice =
                        game.round.as_ref().unwrap().dice[player_name].clone();
                    assert_eq!(player_dice.0.len(), expected_dice_count);

                    let e = Event::RoundStarted {
                        player: player_name.clone(),
                        order: new_order.clone(),
                        dice: player_dice,
                        loser_round,
                    };

                    ev.push(e.clone());
                    admin_evs.push(e);

                    assert_eq!(ev, player_ev.clone());
                    player_ev.clear();
                }

                for ev in admin_evs {
                    assert!(game.admin_events.contains(&ev));
                }
                game.admin_events.clear();
            }
        }
    }
}
