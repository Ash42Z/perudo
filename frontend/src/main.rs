mod admin;
use admin::Admin;

use common::{event::Event, guess::Guess};

use common::api::{GuessParams, JoinGameParams, NextEventsParams, Routes};
use futures_util::stream::StreamExt;
use reqwasm::http::Request;
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_json;
use sycamore::futures::spawn_local_scoped;
use sycamore::prelude::*;
use wasm_timer::Interval;

async fn post(
    route: &Routes,
) -> Result<reqwasm::http::Response, reqwasm::Error> {
    Request::post(route.as_ref()).send().await
}

async fn post_json<Q: Serialize>(
    route: &Routes,
    data: &Q,
) -> Result<reqwasm::http::Response, reqwasm::Error> {
    Request::post(route.as_ref())
        .body(serde_json::to_string(data)?)
        .header("Content-Type", "application/json")
        .send()
        .await
}

async fn post_json_with_response<Q: Serialize, R: DeserializeOwned>(
    route: &Routes,
    data: &Q,
) -> Result<R, reqwasm::Error> {
    post_json(route, data).await?.json().await
}

#[component(inline_props)]
fn DebugEvents<'a, G: Html>(
    cx: Scope<'a>,
    key: &'a ReadSignal<Option<String>>,
) -> View<G> {
    let events = setup_get_events(cx, key);

    view! {cx,
        ul {
            Indexed(
                iterable=events,
                view=|cx, e| view! {cx,
                    li(style="white-space: pre-line") { (e) }
                }
            )
        }
    }
}

fn create_game_event_listener<'a, F, Fut>(
    cx: Scope<'a>,
    key: &'a ReadSignal<Option<String>>,
    cb: F
) 
where F: Fn(Vec<Event>) -> Fut + 'a,
      Fut: std::future::Future<Output = ()>
{
    key.track();
    spawn_local_scoped(cx, async move {
        let mut interval = Interval::new(std::time::Duration::from_secs(1));
        let mut last_ev = 0;

        while interval.next().await.is_some() {
            if let Some(key) = key.get().as_ref() {
                if let Ok(events) =  post_json_with_response::<_, Vec<Event>>(
                    &Routes::NextEvents,
                    &NextEventsParams {
                        key: key.clone(),
                        from: last_ev
                    }
                ).await
                {
                    last_ev += events.len();
                    cb(events).await;
                }
            }
        }
    });
}

fn setup_get_events<'a>(
    cx: Scope<'a>,
    key: &'a ReadSignal<Option<String>>,
) -> &'a ReadSignal<Vec<Event>> {
    let events = create_signal(cx, Vec::<Event>::default());

    create_game_event_listener(cx, key, |new_evs: Vec<Event>| {
        async {
            events.modify().extend(new_evs);
            events.trigger_subscribers();
        }
    });

    events
}

#[derive(Clone)]
struct GameData {
    game_key: String,
    key: String,
}

fn lift_cb<'a>(cx: Scope<'a>, key: &'a ReadSignal<Option<String>>) {
    spawn_local_scoped(cx, async move {
        let Some(key) = key.get().as_ref().clone() else {
            return;
        };

        let _ = post_json(&Routes::LiftCup, &key).await;
    });
}

fn guess_cb<'a>(
    cx: Scope<'a>,
    count: &'a ReadSignal<String>,
    value: &'a ReadSignal<String>,
    key: &'a ReadSignal<Option<String>>,
) {
    spawn_local_scoped(cx, async move {
        let Ok(count) = count.get().parse() else {
            return;
        };

        let Ok(value) = value.get().parse() else {
            return;
        };

        let Some(key) = key.get().as_ref().clone() else {
            return;
        };

        let params = GuessParams {
            key,
            guess: Guess { count, value },
        };

        let _ = post_json(&Routes::Guess, &params).await;
    });
}

#[component]
fn Play<G: Html>(cx: Scope) -> View<G> {
    let game: &Signal<Option<GameData>> = create_signal(cx, None);
    let key = create_memo(cx, || (*game.get()).clone().map(|r| r.key));

    let game_key_input = create_signal(cx, "".to_string());
    let player_name_input = create_signal(cx, "".to_string());

    let count = create_signal(cx, "".to_string());
    let value = create_signal(cx, "".to_string());

    view! {cx, (
        match game.get().as_ref().clone() {
            Some(game) => {
                view! { cx,
                    p { "Game ID: " (game.game_key) }
                    input(bind:value=count, placeholder="Dice amount")
                    input(bind:value=value, placeholder="Dice value")
                    button(on:click=move |_| guess_cb(cx, count, value, key))
                        { "Guess" }
                    button(on:click=move |_| lift_cb(cx, key))
                        { "Lift" }
                    DebugEvents(key=&key)
                }
            }
            None => {
                view! {cx,
                    input(bind:value=game_key_input, placeholder="Room Key")
                    input(
                        bind:value=player_name_input, placeholder="Player Name")
                    button(on:click=move |_| {
                        spawn_local_scoped(cx, async move {
                            let game_key: String =
                                (game_key_input.get().as_ref().clone()).into();
                            let join_params = JoinGameParams {
                                key: game_key.clone(),
                                player_name: (*player_name_input.get()).clone()
                            };

                            if let Ok(key) =
                                post_json_with_response::<_, String>(
                                    &Routes::JoinGame, &join_params).await
                            {
                                game.set(Some(GameData { game_key, key }));
                            }
                        });
                    }) { "Play" }
                }
            }
        }
    )}
}

enum Screen {
    Select,
    Admin,
    Play,
}

#[component(inline_props)]
fn Select<G: Html, 'a>(cx: Scope<'a>, screen: &'a Signal<Screen>) -> View<G> {
    view! {cx,
        button(on:click=|_| screen.set(Screen::Admin)) { "Create" }
        button(on:click=|_| screen.set(Screen::Play)) { "Play" }
    }
}

fn main() {
    sycamore::render(|cx| {
        let screen = create_signal(cx, Screen::Select);

        view! {cx, (
            match screen.get().as_ref() {
                Screen::Select => view! { cx, Select(screen=&screen) },
                Screen::Admin => view! { cx, Admin {} },
                Screen::Play => view! { cx, Play {} }
            }
        )}
    });
}
