use crate::{create_game_event_listener, post, post_json, DebugEvents};

use common::api::{CreateGameResponse, Routes};
use common::dice::Dice;
use common::event::Event;
use common::guess::Guess;

use sycamore::futures::spawn_local_scoped;
use sycamore::prelude::*;

use tokio::sync::Notify;

use std::sync::Arc;

async fn create_game() -> Result<CreateGameResponse, reqwasm::Error> {
    post(&Routes::CreateGame).await?.json().await
}

#[component(inline_props)]
fn StartGameButton<G: Html, 'a>(
    cx: Scope<'a>,
    game_keys: &'a ReadSignal<Option<CreateGameResponse>>,
    started: &'a Signal<bool>,
) -> View<G> {
    view! {cx,
        button(on:click= move |_| {
            game_keys.track();
            spawn_local_scoped(cx, async move {
                if let Some(keys) = &*game_keys.get() {
                    let _ = post_json(
                        &Routes::StartGame,
                        &keys.admin_key
                    ).await;

                    started.set(true);
                }
            });
        }) { "Start" }
    }
}

#[derive(Clone, PartialEq, Eq, Default, Hash)]
struct PlayerProps {
    name: String,
    dice: Dice,
    dice_visible: bool,
    guesses: Vec<Guess>,
    message: Option<String>,
}

fn update_player(
    players: &Signal<Vec<PlayerProps>>,
    name: &str,
    map: impl FnOnce(&mut PlayerProps),
) {
    if let Some(player_id) = players.get().iter().position(|p| p.name == name) {
        map(&mut players.modify()[player_id]);
    }
}

#[component(inline_props)]
fn Player<G: Html, 'a>(
    cx: Scope<'a>,
    player: PlayerProps,
    visibility_override: &'a ReadSignal<bool>,
) -> View<G> {
    let dice = View::new_fragment(
        player
            .dice
            .0
            .iter()
            .copied()
            .map(|die| {
                view! {cx,
                    div(class="w-12 h-12 border-4") {
                        (if *visibility_override.get() || player.dice_visible {
                            view! {cx, span { (die) } }
                        } else {
                            view! {cx, }
                        })
                    }
                }
            })
            .collect(),
    );

    let guesses = View::new_fragment(
        player
            .guesses
            .iter()
            .cloned()
            .map(|guess| {
                view! {cx, span(class="mx-1") { (guess) "," }}
            })
            .collect(),
    );

    view! {cx,
        div(class="flex flex-col") {
            div(class="flex flex-row") {
                span(class="text-xl mx-2") { (player.name) }
                (dice)
                (if let Some(message) = player.message.clone() {
                    view! {cx,
                        span(class="text-xl text-red-600 font-bold") { (message) }
                    }
                } else {
                    view! {cx, }
                })
            }
            div(class="flex flex-row") {
                (guesses)
            }
        }
    }
}

#[component]
pub fn Admin<G: Html>(cx: Scope) -> View<G> {
    let game_keys = create_signal(cx, Option::<CreateGameResponse>::default());

    spawn_local_scoped(cx, async move {
        game_keys.set(create_game().await.ok());
    });

    let started = create_signal(cx, false);
    let players = create_signal(cx, Vec::<PlayerProps>::default());

    let admin_key = create_memo(cx, || {
        (*game_keys.get()).clone().map(|keys| keys.admin_key)
    });

    let event_notify = Arc::new(Notify::new());
    let event_wait = event_notify.clone();

    create_game_event_listener(cx, admin_key, move |new_evs: Vec<Event>| {
        let event_wait = event_wait.clone();
        async move {
            for ev in new_evs {
                match ev {
                    Event::PlayerJoin { name } => {
                        players.modify().push(PlayerProps {
                            name,
                            ..Default::default()
                        });
                    }
                    Event::RoundStarted {
                        player,
                        order,
                        dice,
                        loser_round: _,
                    } => {
                        update_player(
                            players,
                            player.as_str(),
                            move |player| {
                                player.dice = dice;
                                player.dice_visible = false;
                                player.message = None;
                                player.guesses.clear();
                            },
                        );
                        players.modify().sort_by_key(|player| {
                            order
                                .iter()
                                .position(|p| p == &player.name)
                                .unwrap_or(usize::MAX)
                        });

                        event_wait.notified().await;
                    }
                    Event::Guessed { player, guess } => {
                        update_player(
                            players,
                            player.as_str(),
                            move |player| {
                                player.guesses.push(guess);
                            },
                        );
                        event_wait.notified().await;
                    }
                    Event::Lifted {
                        player,
                        dice: _,
                        loser: _,
                    } => {
                        for p in players.modify().iter_mut() {
                            p.dice_visible = true;
                            if p.name == player {
                                p.message = Some("Lift!".into());
                            }
                        }
                        event_wait.notified().await;
                    }
                    Event::Timeout { player } => {
                        update_player(players, player.as_str(), |player| {
                            player.message = Some("I'm very slow!".into());
                        });
                        event_wait.notified().await;
                        update_player(players, player.as_str(), |player| {
                            player.message = None;
                        });
                    }
                    Event::Winner { player } => {
                        update_player(players, player.as_str(), |player| {
                            player.message = Some("I won!".into());
                        });
                    }
                    Event::Chat { player, message } => {
                        update_player(players, player.as_str(), |player| {
                            player.message = Some(message.to_string());
                        });
                        event_wait.notified().await;
                        update_player(players, player.as_str(), |player| {
                            player.message = None;
                        });
                    }
                }
            }
        }
    });

    let visibility_override = create_signal(cx, false);

    view! {cx,
        (if game_keys.get().is_none() {
            view! {cx, "Loading..."}
        } else {
            let event_notify = event_notify.clone();
            view! {cx,
                p { "Game key: " ((*game_keys.get()).as_ref().unwrap().key)  }
                (if !*started.get() {
                    view! {cx, StartGameButton(game_keys=game_keys, started=started)}
                } else {
                    let event_notify = event_notify.clone();
                    view! {cx,
                        button(on:click=move |_| {
                            event_notify.notify_one();
                        }) {
                            ("Next")
                        }
                        input(type="checkbox", bind:checked=visibility_override) {}
                    }
                })
                Keyed(
                    iterable=players,
                    view=move |cx, player| view! { cx,
                        Player(player=player, visibility_override=visibility_override)
                    },
                    key=|player| player.clone()
                )
                details {
                    summary { "Event log" }
                    DebugEvents(key=admin_key)
                }
            }
        }
    )}
}
