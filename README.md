# Perudo

## The rules

### Who is the winner?
The last player with dice in their cup.

### Setup

1. Each player starts with 5 dice.
2. Someone is chosen to go first.

### The basics of a round

1. All players roll the dice, and hide the results using their cup.

2. The first player will need to give a "guess" about the total values of the
dice all players are holding together. For example, a guess of "5 threes" means
the guesser beleives that if we look at all the dice on the table (which are
hidden), **at least** 5 of them are threes.

3. If the next player thinks the guess is wrong, they can lift their cup, which
triggers the end of the round. All players reveal their dice, and whoever was
wrong (the final guesser or the lifter) lose a die, and a new round starts,
with the loser going first. A player who loses their last die is out of the game,
and the next player in the order goes first instead.

4. If the next player does not want to lift their cup, they need to provide a
"higher" guess - by increasing either the value of the die, or the amount.
(So, a guess of "5 threes" can be followed by "5 fours", "5 sixes", "6 twos",
"6 sixes", "8 threes", and so on).

### Ones

The `1` value of the die is a wildcard, and matches **any** guess. This means
that a guess of "5 threes" means that between all dice of all players, at least
5 of them are threes **or ones**.

Because of this property, making a guess with `1` as the die value has some
special rules:

1. Because `1` is a wildcard, guessing ones is a "stronger" guess. Therefore,
a "higher" guess with ones will require **only half** of the amount of dice
of a regular guess. (For example, after a guess of "4 fives", one can guess
"4 sixes", but they can also guess "2 ones". After a guess of "2 ones", the next
lowest legal guess is "5 twos".)

2. You cannot guess "X ones" on the first guess of the round.

### Loser round

A loser round occurs every time a player loses, and is left with one die.
In the following round, after the initial guess, only the **number** of dice
can be changed. (every guess must still be higher than the last).

In a loser round, ones are not wildcards. This means that the round **can** start
with `1` as the value of the guess.

A loser round does not occur if only 2 players are left in the game.

## Getting started

A live server is running [here](https://monkfish-app-6tbck.ondigitalocean.app)

You can browse here and create a new game, or play from the browser.

Try using the example bot!

`python3 bot/main.py https://monkfish-app-6tbck.ondigitalocean.app <Player name> <Game key>`

## API

### Join a game

`POST /join_game`

#### Request

```json
{
    "key": "<Game key>",
    "player_name": "<The name of the new player>"
}
```

#### Response

```json
"<Player key>"
```

### Stream game events

`POST /next_events`

#### Request

```json
{
    "key": "<Player key>",
    "from": <Last event index>
}
```

#### Response

```yaml
[{ <Event> }, { <Event> }, ...]
```

### Make a guess

`POST /guess`

#### Request

```json
{
    "key": "<Player key>",
    "guess": {
        "count": 3,
        "value": 2
    }
```

### Lift the cup

`POST /lift_cup`

#### Request
```json
"<Player key>"
```

### Send a chat message

`POST /chat`

#### Request
```json
{
    "key": "<Player key>",
    "message": { <ChatMessage> }
}
```

### Event

```json
{
    "type" "<Event type>",
    <Event attributes>
}
```

#### Event types

##### Player joined the room

```json
{
    "type": "player_join",
    "name": "<Player name>"
}
```

##### A new round has started

```yaml
{
    "type": "round_started",
    "player": "<Player name>",
    "order": ["<First player>", "<Second player>", ...],
    "dice": [1, 3, 3, 5, 2], # Your dice for this round
    "loser_round": false # Is this a loser round
}
```

##### A player has made a guess

```json
{
    "type": "guessed",
    "player": "<Player name>",
    "guess": {
        "count": <Dice count>,
        "value": <Dice value>
    }
}
```

##### A player has lifted their cup

```json
{
    "type": "lifted",
    "player": "<Player name>",
    "dice": {
        "<Player name>": [1, 3, 3],
        "<Player name>": [4],
        ...
    },
    "loser": "<Loser name>"
}
```

##### A player has won the game

```json
{
    "type": "winner",
    "player": "<Player name>"
}
```

##### A player has sent a chat message

```yaml
{
    "type": "chat",
    "message": { <ChatMessage> }
}
```

### ChatMessage

```json
{
    "type": "<Message type>",
    <Message Attributes>
}
```

#### Message types

##### Give dice info / guess

```json
{
    "type": "dice_info",
    "player": "<Player name>",
    "guess": {
        "count": 3,
        "value": 4
    }
}
```

##### Advice on lifting

```json
{
    "type": "should_lift"
}
```

##### Advice on making a guess

```json
{
    "type": "should_guess",
    "guess": {
        "count": 2,
        "value": 1
    }
}
```

##### A request to focus on a specific player

```json
{
    "type": "focus_on",
    "player": "<Player name>"
}
```

##### Free text message

```json
{
    "type": "text",
    "message": "Some message",
}
```
