FROM rust:latest

RUN rustup target add wasm32-unknown-unknown && cargo install trunk

COPY common/ /common/
COPY backend/ /backend/

RUN mkdir /out \
    && cd /backend \
    && cargo build --release \
    && mv target/release/perudo /out/

COPY frontend/ /frontend/

RUN cd /frontend \
    && trunk build \
    && mv dist /out/

WORKDIR /out
ENTRYPOINT ["./perudo"]
