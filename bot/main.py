import requests
import argparse
import time

class Round:
    def __init__(self, order: list[str], dice: list[int], loser_round: bool):
        self.order = order
        self.dice = dice
        self.loser_round = loser_round
        self.current_turn = 0

    def is_turn(self, name: str):
        return self.order[self.current_turn] == name

    def next_legal_guess(self, count: int, value: int) -> (int, int):
        if self.loser_round:
            return (count + 1, value)

        elif 2 <= value <= 5:
            return (count, value + 1)

        elif value == 6:
            if count % 2 == 0:
                return (count // 2, 1)
            else:
                return (count + 1, 2)
        
        else:
            return (count * 2 + 1, 2)

class Client:
    def __init__(self, host: str, game_key: str, player_name: str):
        self.host = host
        self.key = self.request(
                'join_game',
                {'key': game_key, 'player_name': player_name},
                True
        )
        self.last_event = 0
        self.round = None
        self.name = player_name
        self.dice_tracker = None

    def request(self, path, json={}, ret_json=False, retries=10):
        for _ in range(retries):
            try:
                res = requests.post(f'{self.host}/{path}', json=json)
            except requests.exceptions.ConnectionError:
                time.sleep(1)
                continue

            if res.status_code != 200:
                raise Exception(res.text)

            if ret_json:
                return res.json()
            else:
                return

    def events_forever(self):
        while True:
            evs = self.request(
                    'next_events',
                    {'key': self.key, 'from': self.last_event},
                    True
            )

            self.last_event += len(evs)
            for ev in evs:
                yield ev

            time.sleep(1)

    def handle_new_round(self, order: list[str], dice: list[int], loser_round: bool):
        self.round = Round(order, dice, loser_round)
        if self.dice_tracker is None:
            self.dice_tracker = {name: 5 for name in order}
        if self.round.is_turn(self.name):
            self.request(
                    'guess',
                    {'key': self.key, 'guess': { 'count': 1, 'value': 2} }
            )
            

    def handle_new_guess(self, count: int, value: int):
        self.round.current_turn += 1
        self.round.current_turn %= len(self.round.order)

        if self.round.is_turn(self.name):
            (next_count, next_value) = self.round.next_legal_guess(count, value)
            if self.is_good_guess(next_count, next_value):
                self.request(
                        'guess',
                        {'key': self.key, 'guess': {'count': next_count, 'value': next_value} }
                )
            else:
                self.request('lift_cup', self.key)

    def handle_lifted(self, loser):
        self.dice_tracker[loser] -= 1
        if loser == self.name and self.dice_tracker[loser] == 0:
            print('We have lost. Shame!')
            exit()

    def is_good_guess(self, count: int, value: int) -> bool:
        if self.round.loser_round or value == 1:
            odds_factor = 1 / 6
        else:
            odds_factor = 2 / 6

        total_dice_count = sum(self.dice_tracker.values())

        return total_dice_count * odds_factor >= count


parser = argparse.ArgumentParser()

parser.add_argument('host')
parser.add_argument('player_name')
parser.add_argument('game_key')

args = parser.parse_args()

client = Client(args.host, args.game_key, args.player_name)

for event in client.events_forever():
    print(f'Got ev, {event}')
    if event['type'] == 'round_started':
        client.handle_new_round(event['order'], event['dice'], event['loser_round'])
    elif event['type'] == 'guessed':
        client.handle_new_guess(event['guess']['count'], event['guess']['value'])
    elif event['type'] == 'lifted':
        client.handle_lifted(event['loser'])
    elif event['type'] == 'winner':
        print(f'We won!')
        exit()
    else:
        pass
